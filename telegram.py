import sqlite3
import telebot
import requests
from telebot import types
from telebot.types import InputMediaPhoto
from telebot_calendar import Calendar, CallbackData, RUSSIAN_LANGUAGE
import datetime
from telebot.types import ReplyKeyboardRemove, CallbackQuery

import config
import utils
import hotels
import sql_work


from config_log import logger

bot = telebot.TeleBot(config.TOKEN)

calendar = Calendar(language=RUSSIAN_LANGUAGE)
calendar_1_callback = CallbackData("calendar_1", "action", "year", "month", "day")
calendar_2_callback = CallbackData("calendar_2", "action", "year", "month", "day")


@bot.message_handler(commands=['start'])
def command_start(message: telebot.types.Message) -> None:
    """
    Функция запускает робота
    :param message: сообщение роботу
    :return: None
    """
    logger.info("Function 'command_start' is executed")
    chat_id = message.chat.id
    try:
        sql_work.create_tables(config.HISTORY_BASE)
        if sql_work.is_user_exist(config.HISTORY_BASE, chat_id):
            bot.send_message(chat_id, "И снова здравствуйте!")
            command_help(message)
        else:
            sql_work.add_user(config.HISTORY_BASE, chat_id)
            bot.send_message(chat_id, "Приветствую нового пользователя!")
            command_help(message)
    except (sqlite3.Error, sqlite3.Warning) as error:
        logger.error("Function 'command_start', database access error.")
        bot.send_message(chat_id, error.args[0])
    except Exception as error:
        bot.send_message(chat_id, f"Ошибка в функции 'command_start': {error.args[0]}.\n"
                                  f"Пожалуйста, сообщите о ней разработчику по адресу {config.DEVELOPER_EMAIL}")


@bot.message_handler(commands=['help'])
def command_help(message: telebot.types.Message) -> None:
    """
    Функуция для вывода в телеграм команд робота
    :param message: сообщение роботу
    :return: None
    """
    logger.info("Function 'command_help' is executed")
    sql_work.set_user_step(config.HISTORY_BASE, message.id, config.STEP_START)
    help_text = utils.get_help_menu()
    bot.send_message(message.chat.id, help_text)


@bot.message_handler(commands=['history'])
def command_history(message: telebot.types.Message) -> None:
    """
    Функция для вывода истории поиска отелей в Телеграм
    :param message: сообщение роботу
    :return: None
    """
    logger.info("Function 'command_history' is executed")
    try:
        chat_id = message.chat.id
        request_history = sql_work.get_requests_by_chat_id(config.HISTORY_BASE, chat_id)

        if len(request_history) > 0:
            head = "История поиска отелей\n"
            bot.send_message(chat_id, head)

            for request in request_history:
                # history = f"\n{request[2]}\n"
                history = f"Город: {request[3]}, \n"
                history += f"{config.BOT_COMMAND[request[2]]}\n"
                history += f"Период: {request[6]}\n"
                if request[7].strip():
                    history += f"Расстояние до центра: {request[7]} миль\n"
                if request[10].strip():
                    history += f"Цены: ${request[10]}"
                bot.send_message(chat_id, history)

                hotels = sql_work.get_hotels_by_request_id(config.HISTORY_BASE, request[0])
                for hotel in hotels:
                    hotel_ = hotel[2].replace("-", "\\-").replace(".", "\\.") + '\n'
                    hotel_3 = hotel[3].replace("-", "\\-").replace(".", "\\.")
                    hotel_ += f"Адрес: {hotel_3}"
                    hotel_ += f"\nЦена за сутки: {hotel[6]}"
                    bot.send_message(chat_id, hotel_, parse_mode="MarkdownV2")
        else:
            history = "История поиска пока пустая."
            bot.send_message(chat_id, history)
    except (sqlite3.Error, sqlite3.Warning) as error:
        logger.error("Function 'command_history', database access error.")
        bot.send_message(message.chat.id, error.args[0])
    except Exception as error:
        bot.send_message(message.chat.id, f"Ошибка в функции 'command_history': {error.args[0]}.\n"
                                          f"Пожалуйста, сообщите о ней разработчику по адресу {config.DEVELOPER_EMAIL}")
    command_help(message)


@bot.message_handler(commands=["lowprice", "highprice", "bestdeal"])
def command_lowprice(message: telebot.types.Message) -> None:
    """
    Функция для запроса
    1) самых дешевых отелей,
    2) самых дорогих и
    3) отелей с учетом стоимости и расстояния до центра
    4) Запрос на ввод пользователем города для поиска
    :param message: сообщение роботу
    :return: None
    """
    logger.info("Function 'command_lowprice' is executed")
    try:
        chat_id = message.chat.id
        command = message.html_text[1:]

        request_id = sql_work.set_command(config.HISTORY_BASE, chat_id, command)
        sql_work.set_user_request_id(config.HISTORY_BASE, chat_id, request_id)
        sql_work.set_user_step(config.HISTORY_BASE, chat_id, config.STEP_SELECT_CITIES)

        bot.send_message(chat_id, f"Введите город поиска отеля: ")
    except (sqlite3.Error, sqlite3.Warning) as error:
        logger.error("Function 'command_lowprice', database access error.")
        bot.send_message(message.chat.id, error.args[0])
    except Exception as error:
        bot.send_message(message.chat.id, f"Ошибка в функции 'command_lowprice': {error.args[0]}.\n"
                                          f"Пожалуйста, сообщите о ней разработчику по адресу {config.DEVELOPER_EMAIL}")


@bot.message_handler(func=lambda message: sql_work.get_user_step(config.HISTORY_BASE, message.chat.id) == config.STEP_SELECT_CITIES)
def print_cities(message: telebot.types.Message) -> None:
    chat_id = message.chat.id
    city = str.title(message.text)
    # sql_work.set_city(config.HISTORY_BASE, chat_id, city)   # city
    destinations = hotels.get_destinations(city)

    if not destinations:
        sql_work.set_user_step(config.HISTORY_BASE, chat_id, config.STEP_START)
        bot.send_message(chat_id, "Такого города не нашлось вообще!")
        help_text = utils.get_help_menu()
        bot.send_message(chat_id, help_text)
        return

    sql_work.set_user_step(config.HISTORY_BASE, chat_id, config.STEP_SET_CITY)
    keyboard = types.InlineKeyboardMarkup()
    for destination in destinations:
        caption = destination['caption'].replace("<span class='highlighted'>", "").replace("</span>", "")
        callback_data_ = "%%$$%%".join([destination['id'], caption[0: 10]])
        # button = types.InlineKeyboardButton(text=f"{caption}", callback_data=destination['id'])
        button = types.InlineKeyboardButton(text=f"{caption}", callback_data=callback_data_)
        keyboard.add(button)
    bot.send_message(chat_id, "Уточните ваш выбор:", reply_markup=keyboard)


@bot.callback_query_handler(func=lambda call: not call.data.startswith(calendar_1_callback.prefix)
                                          and not call.data.startswith(calendar_2_callback.prefix)
                                          and not call.data.startswith('yes')
                                          and not call.data.startswith('no')
                                          and not call.data.startswith('Выбраный')
                            )
def set_city(call: telebot.types.CallbackQuery) -> None:
    logger.info("Function 'set_city' is executed")
    try:
        command_low_price = "lowprice"
        command_high_price = "highprice"

        chat_id = call.from_user.id
        destination_id, city = call.data.split("%%$$%%")

        sql_work.set_destination_id(config.HISTORY_BASE, chat_id, destination_id)
        sql_work.set_city(config.HISTORY_BASE, chat_id, city)
        bot.send_message(chat_id, f"Город выбран.")

        command_current = sql_work.get_command(config.HISTORY_BASE, chat_id)
        if command_current in [command_low_price, command_high_price]:
            now = datetime.datetime.now()
            bot.send_message(
                chat_id,
                "Выберите начальную дату:",
                reply_markup=calendar.create_calendar(
                    name=calendar_1_callback.prefix,
                    year=now.year,
                    month=now.month,
                )
            )
        else:
            sql_work.set_user_step(config.HISTORY_BASE, chat_id, config.STEP_SET_PRICES)
            bot.send_message(chat_id, "Введите диапазон цен за сутки (ХХ - ХХ) в USD:")
    except (sqlite3.Error, sqlite3.Warning) as error:
        logger.error("Function 'set_city', database access error.")
        bot.send_message(call.message.text, error.args[0])
    except Exception as error:
        bot.send_message(call.message.chat.id, f"Ошибка в функции 'set_city': {error.args[0]}.\n"
                         f"Пожалуйста, сообщите о ней разработчику по адресу {config.DEVELOPER_EMAIL}")


@bot.message_handler(func=lambda message: sql_work.get_user_step(config.HISTORY_BASE, message.chat.id) == config.STEP_SET_CITY)
def set_city(message: telebot.types.Message) -> None:
    """
    Функция для
    1) вывода выбраного пользователем города в телеграм,
    2) запрос на ввод стартовой и финальной дат (для команд "lowprice" и "highprice")
    3) запроса на ввод диапазона цен за сутку (для команды "bestdeal")
    :param message: сообщение роботу
    :return: None
    """
    logger.info("Function 'set_city' is executed")
    try:
        command_low_price = "lowprice"
        command_high_price = "highprice"

        chat_id = message.chat.id
        city = str.title(message.text)
        sql_work.set_city(config.HISTORY_BASE, chat_id, city)

        command_current = sql_work.get_command(config.HISTORY_BASE, chat_id)
        if command_current == command_low_price or command_current == command_high_price:
            now = datetime.datetime.now()
            bot.send_message(
                chat_id,
                "Выберите начальную дату:",
                reply_markup=calendar.create_calendar(
                    name=calendar_1_callback.prefix,
                    year=now.year,
                    month=now.month,
                )
            )
        else:
            sql_work.set_user_step(config.HISTORY_BASE, chat_id, config.STEP_SET_PRICES)
            bot.send_message(chat_id, "Введите диапазон цен за сутки (ХХ - ХХ) в USD:")
    except (sqlite3.Error, sqlite3.Warning) as error:
        logger.error("Function 'set_city', database access error.")
        bot.send_message(message.chat.id, error.args[0])
    except Exception as error:
        bot.send_message(message.chat.id, f"Ошибка в функции 'set_city': {error.args[0]}.\n"
                                          f"Пожалуйста, сообщите о ней разработчику по адресу {config.DEVELOPER_EMAIL}")


@bot.message_handler(func=lambda message: sql_work.get_user_step(config.HISTORY_BASE, message.chat.id) == config.STEP_SET_PRICES)
def set_prices(message: telebot.types.Message) -> None:
    """
    Функция для
        1) сохранения диапазона цен отелей
        2) вывода диапазона цен в телеграм
        3) запроса на ввод пользователем дипазона расстояний до отеля
    :param message: сообщение роботу
    :return: None
    """
    logger.info("Function 'set_prices' is executed")
    try:
        chat_id = message.chat.id
        prices = message.text

        if not utils.is_pair_ordered(prices):
            bot.send_message(chat_id, "Некорректные цены, введите еше раз (ХХ - ХХ):")
            return

        sql_work.set_user_step(config.HISTORY_BASE, chat_id, config.STEP_SET_DISTANCES)
        sql_work.set_prices(config.HISTORY_BASE, chat_id, prices)

        bot.send_message(chat_id, f"Выбраный диапозон цен: {prices}")
        bot.send_message(chat_id, "Введите диапзон расстояния от отеля до центра (ХХ - ХХ) в милях: ")
    except (sqlite3.Error, sqlite3.Warning) as error:
        logger.error("Function 'set_prices', database access error.")
        bot.send_message(message.chat.id, error.args[0])
    except Exception as error:
        bot.send_message(message.chat.id, f"Ошибка в функции 'set_prices': {error.args[0]}.\n"
                                          f"Пожалуйста, сообщите о ней разработчику по адресу {config.DEVELOPER_EMAIL}")


@bot.message_handler(func=lambda message: sql_work.get_user_step(config.HISTORY_BASE, message.chat.id) == config.STEP_SET_DISTANCES)
def set_distances(message: telebot.types.Message) -> None:
    """
    Функция для
    1) сохранения диапазона расстояний до отеля
    2) запроса на ввод пользователем начальной даты пребывания в отеле
    :param message: сообщение роботу
    :return: None
    """
    logger.info("Function 'set_distances' is executed")
    try:
        chat_id = message.chat.id
        distances = message.text

        if not utils.is_pair_ordered(distances):
            bot.send_message(chat_id, "Некорректные расcтояния, введите еше раз (ХХ - ХХ):")
            return

        bot.send_message(chat_id, f"Выбраный диапазон расстояний: {distances}")
        sql_work.set_distances(config.HISTORY_BASE, chat_id, distances)

        now = datetime.datetime.now()
        bot.send_message(
            chat_id,
            "Выберите начальную дату:",
            reply_markup=calendar.create_calendar(
                name=calendar_1_callback.prefix,
                year=now.year,
                month=now.month,
            )
        )
    except (sqlite3.Error, sqlite3.Warning) as error:
        logger.error("Function 'set_distance', database access error.")
        bot.send_message(message.chat.id, error.args[0])
    except Exception as error:
        bot.send_message(message.chat.id, f"Ошибка в функции 'set_distances': {error.args[0]}.\n"
                                          f"Пожалуйста, сообщите о ней разработчику по адресу {config.DEVELOPER_EMAIL}")


@bot.callback_query_handler(func=lambda call: call.data.startswith(calendar_1_callback.prefix))
def callback_inline(call: CallbackQuery):
    """
    Функция
    1) обрабатывает запрос на ввод начальной даты поиска отеля
    2) запрашивает конечную дату поиска отелей
    :param call: Обратный запрос
    :return: None
    """
    logger.info("Function 'callback_inline'(set start date) is executed")
    try:
        name, action, year, month, day = call.data.split(calendar_1_callback.sep)
        date = calendar.calendar_query_handler(
            bot=bot, call=call, name=name, action=action, year=year, month=month, day=day
        )
        if action == "DAY":
            bot.send_message(
                chat_id=call.from_user.id,
                text=f"Начало: {date.strftime('%d.%m.%Y')}",
                reply_markup=ReplyKeyboardRemove(),
            )
            # Записываю первую дату в базу
            chat_id = call.from_user.id
            dates = date.strftime('%d.%m.%Y')
            sql_work.set_dates(config.HISTORY_BASE, chat_id, dates)

            now = datetime.datetime.now()
            bot.send_message(
                call.message.chat.id,
                "Выберите конечную дату:",
                reply_markup=calendar.create_calendar(
                    name=calendar_2_callback.prefix,
                    year=now.year,
                    month=now.month,
                )
            )
        elif action == "CANCEL":
            sql_work.set_user_step(config.HISTORY_BASE, call.from_user.id, config.STEP_START)
            chat_id = call.from_user.id
            help_text = utils.get_help_menu()
            bot.send_message(chat_id, help_text)
    except (sqlite3.Error, sqlite3.Warning) as error:
        logger.error("Function 'callback_inline'(set start date), database access error.")
        bot.send_message(call.from_user.id, error.args[0])
    except Exception as error:
        bot.send_message(call.from_user.id, f"Ошибка в функции, запрашивающей вторую дату: {error.args[0]}.\n"
                                            f"Пожалуйста, сообщите о ней разработчику по адресу {config.DEVELOPER_EMAIL}")


@bot.callback_query_handler(func=lambda call: call.data.startswith(calendar_2_callback.prefix))
def callback_inline(call: CallbackQuery) -> None:
    """
    Обработка inline callback запросов
    Функция
    1) обрабатывает запрос на ввод конечной даты поиска отеля
    2) вызывает функцию set_dates
    :param call: Обратный  вызов
    :return: None
    """
    logger.info("Function 'callback_inline'(set final date) is executed")
    try:
        name, action, year, month, day = call.data.split(calendar_1_callback.sep)
        date = calendar.calendar_query_handler(
            bot=bot, call=call, name=name, action=action, year=year, month=month, day=day
        )

        if action == "DAY":
            bot.send_message(
                chat_id=call.from_user.id,
                text=f"Конец: {date.strftime('%d.%m.%Y')}",
                reply_markup=ReplyKeyboardRemove(),
            )

            chat_id = call.from_user.id
            dates_begin = sql_work.get_dates(config.HISTORY_BASE, chat_id)
            dates = dates_begin + "-" + date.strftime('%d.%m.%Y')
            sql_work.set_dates(config.HISTORY_BASE, chat_id, dates)

            set_dates(chat_id)

        elif action == "CANCEL":
            sql_work.set_user_step(config.HISTORY_BASE, call.from_user.id, config.STEP_START)
            chat_id = call.from_user.id
            help_text = utils.get_help_menu()
            bot.send_message(chat_id, help_text)
    except (sqlite3.Error, sqlite3.Warning) as error:
        logger.error("Function 'callback_inline'(set final date), database access error.")
        bot.send_message(call.from_user.id, error.args[0])
    except Exception as error:
        bot.send_message(call.from_user.id, f"Ошибка в функции, сохраняющей вторую дату: {error.args[0]}.\n"
                                            f"Пожалуйста, сообщите о ней разработчику по адресу {config.DEVELOPER_EMAIL}")


def set_dates(chat_id: int) -> None:
    """
    Функция для
    1) сохранения диапазона дат пребывания в отеле
    2) запроса на ввод пользователем количества отелей
    :param chat_id: номер пользователя
    :return: None
    """
    logger.info("Function 'set_dates' is executed")
    try:
        dates = sql_work.get_dates(config.HISTORY_BASE, chat_id)

        if not utils.is_dates_correct(dates):
            bot.send_message(chat_id, "Некорректные даты, пожалуйста, введите ешё раз")

            now = datetime.datetime.now()
            bot.send_message(
                chat_id,
                "Выберите начальную дату:",
                reply_markup=calendar.create_calendar(
                    name=calendar_1_callback.prefix,
                    year=now.year,
                    month=now.month,
                )
            )
            return

        sql_work.set_user_step(config.HISTORY_BASE, chat_id, config.STEP_SET_HOTELS_AMOUNT)

        bot.send_message(chat_id, f"Выбраные даты: {dates}")
        bot.send_message(chat_id, f"Введите кол-во отелей (от {config.HOTELS_AMOUNT_MIN} до {config.HOTELS_AMOUNT_MAX}):")
    except (sqlite3.Error, sqlite3.Warning) as error:
        logger.error("Function 'set_dates', database access error.")
        bot.send_message(chat_id, error.args[0])
    except Exception as error:
        bot.send_message(chat_id, f"Ошибка в функции 'set_dates': {error.args[0]}.\n"
                                  f"Пожалуйста, сообщите о ней разработчику по адресу {config.DEVELOPER_EMAIL}")


@bot.message_handler(func=lambda message: sql_work.get_user_step(config.HISTORY_BASE, message.chat.id) == config.STEP_SET_HOTELS_AMOUNT)
def set_hotels_amount(message: telebot.types.Message) -> None:
    """
    Функция для
    1) сохранения количества отелей
    2) запроса на ввод пользователем необходимости вывода фотографий
    :param message: сообщение роботу
    :return: None
    """
    logger.info("Function 'set_hotels_amount' is executed")
    try:
        chat_id = message.chat.id
        hotels_amount = message.text

        if not utils.is_number_in_range(hotels_amount, config.HOTELS_AMOUNT_MIN, config.HOTELS_AMOUNT_MAX):
            bot.send_message(chat_id, f"Некорректно кол-во отелей, пожалуйста введите ещё раз"
                                      f" (от {config.HOTELS_AMOUNT_MIN} до {config.HOTELS_AMOUNT_MAX})")
            return

        bot.send_message(chat_id, f"Кол-во отелей: {hotels_amount}")

        sql_work.set_hotels_amount(config.HISTORY_BASE, chat_id, int(hotels_amount))
        if_want_to_get_photo(chat_id)
    except (sqlite3.Error, sqlite3.Warning) as error:
        logger.error("Function 'set_hotels_amount', database access error.")
        bot.send_message(message.chat.id, error.args[0])
    except Exception as error:
        bot.send_message(message.chat.id, f"Ошибка в функции 'set_hotels_amount': {error.args[0]}.\n"
                                          f"Пожалуйста, сообщите о ней разработчику по адресу {config.DEVELOPER_EMAIL}")


def if_want_to_get_photo(chat_id: int | str) -> None:
    """
    Функция спрашивает: надо ли выводить фото
    :param chat_id: номер пользователя
    :return: None
    """
    logger.info("Function 'if_want_to_get_photo' is executed")
    try:
        keyboard = types.InlineKeyboardMarkup()
        callback_button_yes = types.InlineKeyboardButton(text="С фотографиями", callback_data="yes")
        callback_button_no = types.InlineKeyboardButton(text="Без фотографий", callback_data="no")
        keyboard.add(callback_button_yes)
        keyboard.add(callback_button_no)
        bot.send_message(chat_id, "Хотите посмотреть фото отеля?", reply_markup=keyboard)
    except Exception as error:
        bot.send_message(chat_id, f"Ошибка в функции 'if_want_to_get_photo': {error.args[0]}.\n"
                                  f"Пожалуйста, сообщите о ней разработчику по адресу {config.DEVELOPER_EMAIL}")


@bot.callback_query_handler(func=lambda call: call.data.startswith('yes') or call.data.startswith('no'))
def set_pictures(call: telebot.types.CallbackQuery) -> None:
    """
    Функция либо для
    1) запроса на ввод пользователем количества фотографий
    либо
    2) запроса к сайту hotels.com и вывода полученых данных в телеграм
    :param call: обратный вызов
    :return: None
    """
    logger.info("Function 'set_pictures' is executed")
    try:
        chat_id = call.message.chat.id
        if call.data == "yes":
            sql_work.set_user_step(config.HISTORY_BASE, chat_id, config.STEP_SET_PICTURE_AMOUNT)
            bot.send_message(chat_id, f"Сколько фото для каждого отеля выводить? "
                                      f"(от {config.PICTURE_AMOUNT_MIN} до {config.PICTURE_AMOUNT_MAX}): ")
        else:
            sql_work.set_picture_amount(config.HISTORY_BASE, chat_id, 0)
            get_hotels(chat_id)
    except (sqlite3.Error, sqlite3.Warning) as error:
        logger.error("Function 'set_pictures', database access error.")
        bot.send_message(call.message.chat.id, error.args[0])
    except Exception as error:
        bot.send_message(call.message.chat.id, f"Ошибка в функции 'set_hotels_amount': {error.args[0]}.\n"
                         f"Пожалуйста, сообщите о ней разработчику по адресу {config.DEVELOPER_EMAIL}")


@bot.message_handler(func=lambda message: sql_work.get_user_step(config.HISTORY_BASE, message.chat.id) ==
                     config.STEP_SET_PICTURE_AMOUNT)
def set_picture_amount(message: telebot.types.Message) -> None:
    """
    Функция для
    1) сохранения количества фотографий
    2) запроса к сайту hotels.com и вывода полученых данных в телеграм
    :param message: сообщение роботу
    :return: None
    """
    logger.info("Function 'set_picture_amount' is executed")
    try:
        chat_id = message.chat.id
        picture_amount = message.text

        if not utils.is_number_in_range(picture_amount, config.PICTURE_AMOUNT_MIN, config.PICTURE_AMOUNT_MAX):
            bot.send_message(chat_id, f"Некорректно кол-во фотографий, пожалуйста введите ещё раз "
                                      f"(от {config.PICTURE_AMOUNT_MIN} до {config.PICTURE_AMOUNT_MAX})")
            return

        sql_work.set_picture_amount(config.HISTORY_BASE, chat_id, int(picture_amount))
        get_hotels(chat_id)
    except (sqlite3.Error, sqlite3.Warning) as error:
        logger.error("Function 'set_picture_amount', database access error.")
        bot.send_message(message.chat.id, error.args[0])
    except Exception as error:
        bot.send_message(message.chat.id, f"Ошибка в функции 'set_picture_amount': {error.args[0]}.\n"
                                          f"Пожалуйста, сообщите о ней разработчику по адресу {config.DEVELOPER_EMAIL}")


def get_hotels(chat_id: int) -> None:
    """
    Функция для запроса к сайту hotels.com и вывода полученых данных в телеграм
    :param chat_id: id пользователя
    :return: None
    """
    logger.info("Function 'get_hotels' is executed")
    try:
        request_params = sql_work.get_request_current(config.HISTORY_BASE, chat_id)
        command = request_params[0]
        dates = request_params[1]
        city = request_params[2]
        destination_id = request_params[3] #
        prices = request_params[4]
        hotels_amount = request_params[5]
        picture_amount = request_params[6]
        distances = request_params[7]

        checkin_date, checkout_date = utils.to_convert_dates_string(dates)
        price_min, price_max = utils.convert_to_pair(prices, config.PRICE_MIN, config.PRICE_MAX)
        distance_min, distance_max = utils.convert_to_pair(distances, config.DISTANCE_MIN, config.DISTANCE_MAX)

        bot.send_message(chat_id, "Поиск начался, дождитесь его окончания...")

        if destination_id:
            hotels_list = hotels.get_hotels_by_request(headers=config.HEADERS, city=city, checkin_date=checkin_date,
                                                       checkout_date=checkout_date, price_min=price_min,
                                                       price_max=price_max, price_sorting=command,
                                                       hotels_amount=hotels_amount, distance_min=distance_min,
                                                       distance_max=distance_max,
                                                       chat_id=str(chat_id),
                                                       destination_id=destination_id)

            for hotel in hotels_list:
                sql_work.set_hotel(config.HISTORY_BASE, chat_id, hotel["name"], hotel["address"],
                                   hotel["landmark"], hotel["urls"], hotel["price_one_day"],
                                   hotel["price_total"], hotel["id"])

            if picture_amount:
                for hotel in hotels_list:
                    hotel_id = hotel["id"]
                    hotel_photos = hotels.get_hotels_photos_by_request(str(hotel_id), picture_amount)
                    hotel["photos"] = hotel_photos

            send_hotels_to_telegram(chat_id, hotels_list)
            if not len(hotels_list):
                bot.send_message(chat_id, "По вашему запросу ничего не найдено.")
            else:
                bot.send_message(chat_id, "Поиск завершен.")
        else:
            bot.send_message(chat_id, "По вашему запросу ничего не найдено.")
        sql_work.set_user_step(config.HISTORY_BASE, chat_id, config.STEP_START)
        help_text = utils.get_help_menu()
        bot.send_message(chat_id, help_text)
    except IndexError as error:
        bot.send_message(chat_id, error.args[0])
    except KeyError as error:
        bot.send_message(chat_id, error.args[0])
    except requests.exceptions.RequestException as error:
        bot.send_message(chat_id, error.args[0])
    except (sqlite3.Error, sqlite3.Warning) as error:
        bot.send_message(chat_id, error.args[0])
    except Exception as error:
        bot.send_message(chat_id, f"Ошибка в функции 'get_hotels': {error.args[0]}.\n"
                                  f"Пожалуйста, сообщите о ней разработчику по адресу {config.DEVELOPER_EMAIL}")


def send_hotels_to_telegram(chat_id: int, hotels_list: list) -> None:
    """
    Функция отправляет сообщения в Telegram с результатами поиска отелей
    :param chat_id: номер пользователя
    :param hotels_list: - список отелей
    :return: None
    """
    logger.info("Function 'send_hotels_to_telegram' is executed")
    fields_name = {
        "name": "Отель",
        "address": "Адрес",
        "landmark": "Расстояние до центра",
        "price_one_day": "Цена за сутки",
        "price_total": "Цена общая",
    }
    try:
        hotels_counter = 1
        for hotel in hotels_list:
            hotels_in_one_string = f"\n\n№ {hotels_counter}\n"
            result_tmp = '\n'.join([f'{fields_name[key]}: {value}' for key, value in hotel.items()
                                    if key in fields_name.keys()])
            hotels_in_one_string += result_tmp
            hotels_counter += 1
            bot.send_message(chat_id, hotels_in_one_string,
                             reply_markup=types.ReplyKeyboardRemove(),
                             parse_mode='Markdown')

            media_group = []
            for hotels_picture_address in hotel.get("photos", list()):
                media_group.append(InputMediaPhoto(hotels_picture_address, caption=''))
            if len(media_group):
                bot.send_media_group(chat_id, media_group)
    except (sqlite3.Error, sqlite3.Warning) as error:
        logger.error("Function 'send_hotels_to_telegram', database access error.")
        bot.send_message(chat_id, error.args[0])
    except Exception as error:
        bot.send_message(chat_id, f"Ошибка в функции 'send_hotels_to_telegram': {error.args[0]}.\n"
                                  f"Пожалуйста, сообщите о ней разработчику по адресу {config.DEVELOPER_EMAIL}")


@bot.message_handler(func=lambda message: True, content_types=['text'])
def command_default(message) -> None:
    """
    Стандартный ответ
    :param message:  сообщение роботу
    :return: None
    """
    logger.info("Function 'command_default' is executed")
    bot.send_message(message.chat.id,
                     f"Неизвестная команда: {message.text} \nПосмотрите справку по командам: /help")


bot.infinity_polling()
