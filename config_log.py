# from config_log import get_logging_config
from logging.config import dictConfig
import logging

def get_logging_config(info_log_file: str, error_log_file: str):
    logging_config = {
        'version': 1,
        'disable_existing_loggers': False,
        'loggers': {
            'root': {  # root logger
                'level': 'NOTSET',
                'handlers': ['debug_console_handler', 'info_rotating_file_handler', 'error_file_handler', 'critical_mail_handler'],
            },
            'my.package': {
                'level': 'INFO',
                'propagate': False,
                'handlers': ['debug_console_handler', 'info_rotating_file_handler', 'error_file_handler'],
            },
        },
        'root': {},
        'handlers': {
            'debug_console_handler': {
                'level': 'DEBUG',
                'formatter': 'info',
                'class': 'logging.StreamHandler',
                'stream': 'ext://sys.stdout',
            },
            'info_rotating_file_handler': {
                'level': 'INFO',
                'formatter': 'info',
                'class': 'logging.handlers.RotatingFileHandler',
                'filename': info_log_file,
                'mode': 'a',
                'maxBytes': 1048576,
                'backupCount': 10
            },
            'error_file_handler': {
                'level': 'WARNING',
                'formatter': 'error',
                'class': 'logging.FileHandler',
                'filename': error_log_file,
                'mode': 'a',
            },
            'critical_mail_handler': {
                'level': 'CRITICAL',
                'formatter': 'error',
                'class': 'logging.handlers.SMTPHandler',
                'mailhost': 'localhost',
                'fromaddr': 'niklas1077@yandex.ru',
                'toaddrs': ['niklas1077@yandex.ru', 'niklas1077@yandex.ru'],
                'subject': 'Critical error with application name'
            }
        },
        'formatters': {
            'info': {
                'format': '%(asctime)s-%(levelname)s-%(name)s::%(module)s|%(lineno)s:: %(message)s'
            },
            'error': {
                'format': '%(asctime)s-%(levelname)s-%(name)s-%(process)d::%(module)s|%(lineno)s:: %(message)s'
            },
        },
    }
    return logging_config

logger_config = get_logging_config("info.log", "error.log")
logging.config.dictConfig(logger_config)
logger = logging.getLogger('my.package')