import datetime
import re

from config import BOT_COMMAND
from config import DEVELOPER_EMAIL


def is_dates_correct(dates_start_end: str) -> bool:
    """"
    Проверка корректности введеных дат заказа
    (dd.mm.yyyy - dd.mm.yyyy или dd/mm/yyyy - dd/mm/yyyy или dd-mm-yyyy - dd-mm-yyyy),
    первая дата меньше второй
    :param dates_start_end: начальная и конечная дата
    :return: если первая дата меньше второй, то истина
    """
    try:
        pattern = r"([12][0-9]|3[0-1]|0?[1-9])([./-])(1[0-2]|0?[1-9])([./-])(2?1?[0-9][0-9][0-9])"
        dd_mm_yyyy = re.findall(pattern, dates_start_end)

        date_start = ".".join(dd_mm_yyyy[0][::2])
        date_end = ".".join(dd_mm_yyyy[1][::2])
        date_start = datetime.datetime.strptime(date_start, '%d.%m.%Y').date()
        date_end = datetime.datetime.strptime(date_end, '%d.%m.%Y').date()
        now = datetime.date.today()

        if now <= date_start < date_end:
            return True
    except Exception:
        return False
    return False


def get_days_amount(date_start: str,  date_end: str) -> int:
    """
    Функция возращает количество дней между двумя датами.
    Даты в формате строки типа YYYY-mm-dd
    :param date_start: начальная дата
    :param date_end: конечная дата
    :return: количество дней между датами
    """
    try:
        date_start = datetime.datetime.strptime(date_start, '%Y-%m-%d')
        date_end = datetime.datetime.strptime(date_end, '%Y-%m-%d')
        days_amount = (date_end - date_start).days
        return days_amount
    except ValueError as error:
        raise ValueError(f"Ошибка в функции 'get_days_amount': : {error.args[0]}.\n"
                         f"Пожалуйста, сообщите о ней разработчику по адресу {DEVELOPER_EMAIL}.") from error


def to_convert_dates_string(dates_start_end: str) -> tuple:
    """
    Функция преобразует строку типа
    "dd.mm.YYYY - dd.mm.YYYY" или "dd/mm/YYYY - dd/mm/YYYY" или "dd-mm-YYYY - dd-mm-YYYY" в
    кортеж ("YYYY-mm-dd", "YYYY-mm-dd")
    :param dates_start_end:  строка с диапазоном дат
    :return: кортеж (начало и конец диапазона)
    """
    try:
        pattern = r"([12][0-9]|3[0-1]|0?[1-9])([./-])(1[0-2]|0?[1-9])([./-])(2?1?[0-9][0-9][0-9])"
        dd_mm_yyyy = re.findall(pattern, dates_start_end)

        date_start = "-".join(dd_mm_yyyy[0][-1::-2])
        date_end = "-".join(dd_mm_yyyy[1][-1::-2])
        return date_start, date_end
    except IndexError as error:
        raise ValueError(f"Ошибка в функции 'to_convert_dates_string': : {error.args[0]}.\n"
                         f"Пожалуйста, сообщите о ней разработчику по адресу {DEVELOPER_EMAIL}.") from error


def convert_to_pair(pair: str, one: float = 0, two: float = 1000000) -> tuple:
    """
    Функция преобразует строку с двумя числами разделеными тире или пробелом (и т.п.) в кортеж.
    Если преобразовать не удалось, то возвращает кортеж с числами one, two
    :param pair: строка ХХХ - ХХХ
    :param one: первое число
    :param two: второе число
    :return: кортеж и двух чисел
    """
    try:
        pattern = r"\d+"
        distances = re.findall(pattern, pair)
        return int(distances[0]), int(distances[1])
    except Exception:
        return one, two


def is_pair_ordered(pair: str) -> bool:
    """
    Проверка корректности введеных расстояний до отеля (оба положительные, второе больше или равно первому)
    Входной диапазон вводится как строка с двумя числами разделеная тире или пробелом (и т.п.)
    :param pair: Строка с парой чисел  ("123 - 456")
    :return: если даты в строке упорядочены по возрастанию и больше нуля, то истина
    """
    try:
        pattern = r"\d+"
        distances = re.findall(pattern, pair)
        if 0 <= float(distances[0]) <= float(distances[1]):
            return True
    except Exception:
        return False
    return False


def is_number_in_range(number: str, number_min: int, number_max: int) -> bool:
    """
    Проверка корректности ввода значения (целое неотицательное число укладывается в диапазон  значнений)
    :param number: проверяемое число
    :param number_min: нижняя граница диапазона
    :param number_max: верхняя граница диапазон
    :return: если число входит в диапазон (включая граниы) то истина
    """
    try:
        number = int(number)
        if number_min <= number <= number_max:
            return True
    except Exception:
        return False
    return False


def get_help_menu() -> str:
    """
    Меню Помощь по командам робота
    :return: строка с командами меню
    """
    help_text = "Доступны следующие команды: \n"
    for key, value in BOT_COMMAND.items():
        help_text += "/" + key + ": "
        help_text += value + "\n"
    return help_text
