TOKEN = "5386337310:AAFU_lquR4baEOkgaX6lMLjeh_3arQIWQMs"
HISTORY_BASE = 'history.db'

BOT_COMMAND = {
    "start": "Запустить бота",
    "help": "Справка по командам бота",
    "lowprice": "Самые дешевые отели",
    "highprice": "Самые дорогие отели",
    "bestdeal": "Отели, наиболее подходящие по цене и расстоянию от центра",
    "history": "Вывод истории поиска отелей"
}

STEP_START = 0
STEP_SET_CITY = 1
STEP_SET_PRICES = 22
STEP_SET_DISTANCES = 33
STEP_SET_HOTELS_AMOUNT = 3
STEP_SET_PICTURE_AMOUNT = 5
STEP_SELECT_CITIES = 6

HOTELS_AMOUNT_MIN = 1
HOTELS_AMOUNT_MAX = 10
PICTURE_AMOUNT_MIN = 1
PICTURE_AMOUNT_MAX = 5


X_RAPIDAPI_HOST = "hotels-com-provider.p.rapidapi.com"
X_RAPIDAPI_KEY = "9ddf3fe194msh7356c5d330c17c5p1bebf1jsnaf4aa8cd868f"

HEADERS = {
    "X-RapidAPI-Host": X_RAPIDAPI_HOST,
    "X-RapidAPI-Key": X_RAPIDAPI_KEY
}

URL_SEARCH_HOTELS = "https://hotels-com-provider.p.rapidapi.com/v1/hotels/search"
URL_DESTINATION = "https://hotels-com-provider.p.rapidapi.com/v1/destinations/search"
URL_NEARBY = "https://hotels-com-provider.p.rapidapi.com/v1/hotels/nearby"
URL_REVIEWS = "https://hotels-com-provider.p.rapidapi.com/v1/hotels/reviews"
URL_BOOKING_DETAILS = "https://hotels-com-provider.p.rapidapi.com/v1/hotels/booking-details"
URL_PHOTOS = "https://hotels-com-provider.p.rapidapi.com/v1/hotels/photos"

DISTANCE_MIN = 0
DISTANCE_MAX = 1000000

PRICE_MIN = 0
PRICE_MAX = 1000000

DEVELOPER_EMAIL = "email@yandex.ru"
