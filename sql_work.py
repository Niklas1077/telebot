import sqlite3
from config_log import logger
from typing import Callable
from config import DEVELOPER_EMAIL


def sql_error_handling(func) -> Callable:
    def inner_wrapper(*args, **kwargs):
        try:
            value = func(*args, **kwargs)
        except (sqlite3.Error, sqlite3.Warning) as error:
            logger.error(f"Function '{func.__name__}, {error.args[0]}'")
            raise sqlite3.Error(f"Ошибка в функции '{func.__name__}'.\n"
                                f"Пожалуйста, сообщите о ней разработчику по адресу {DEVELOPER_EMAIL}.\n"
                                f"Для продолжения работы наберите комманду /start") from error
        return value
    return inner_wrapper


def connect_to_base(hotels_base: str) -> sqlite3.Connection:
    logger.info("Function 'connect_to_base' is executed")
    try:
        connect_ = sqlite3.connect(hotels_base)
        return connect_
    except (sqlite3.Error, sqlite3.Warning) as error:
        logger.error(f"Function 'connect_to_base', {error.args[0]}.")
        raise sqlite3.Error(f"Error in function 'connect_to_base': {error.args[0]}.") from error


def execute_sql_command(cursor_: sqlite3.Cursor, command_sql_: str) -> None:
    logger.info("Function 'execute_sql_command' is executed")
    try:
        cursor_.execute(command_sql_)
    except (sqlite3.Error, sqlite3.Warning) as error:
        logger.error(f"Function 'execute_sql_command', {error.args[0]}.")
        raise sqlite3.Error(f"Error in function 'execute_sql_command': {error.args[0]}.") from error


@sql_error_handling
def create_tables(hotels_base: str) -> None:
    """
    Функция создает таблицы в базе "hotels_base" если они еще не существуют
    :param hotels_base: файл базы данных
    :return: None
    """
    logger.info("Function 'create_tables' is executed")
    connect = connect_to_base(hotels_base)
    # user
    with connect:
        cursor = connect.cursor()
        command_sql = """
                CREATE TABLE IF NOT EXISTS users (
                    id INTEGER PRIMARY KEY,
                    chat_id INTEGER,
                    step_current INTEGER,
                    request_id INTEGER
                );
                """
        execute_sql_command(cursor, command_sql)

    # requests
    with connect:
        cursor = connect.cursor()
        command_sql = """
                CREATE TABLE IF NOT EXISTS requests (
                    id INTEGER PRIMARY KEY,
                    chat_id INTEGER,
                    command TEXT NOT NULL,
                    city TEXT,
                    destination_id TEXT,
                    date_command TEXT,
                    dates TEXT,
                    distances TEXT DEFAULT '0-1000000',
                    hotels_amount INT,
                    picture_amount INT,
                    prices TEXT DEFAULT '0-1000000'
                )
                """
        execute_sql_command(cursor, command_sql)

    # hotels
    with connect:
        cursor = connect.cursor()
        command_sql = """
                CREATE TABLE IF NOT EXISTS hotels (
                    id INTEGER PRIMARY KEY,
                    request_id INTEGER NOT NULL,
                    name TEXT,
                    address TEXT,
                    landmark TEXT,
                    urls TEXT,
                    price_one_day TEXT,
                    price_total TEXT,
                    id_hotel INTEGER
                )
                """
        execute_sql_command(cursor, command_sql)


@sql_error_handling
def is_user_exist(hotels_base: str, chat_id: int) -> bool:
    """
    Функция возвращает True, если  пользователь с chat_id уже существует
    :param hotels_base: файл базы данных
    :param chat_id: id чата
    :return: True если пользователь есть в базе
    """
    logger.info("Function 'is_user_exist' is executed")
    connect = connect_to_base(hotels_base)
    with connect:
        cursor = connect.cursor()
        command_sql = f"""SELECT users.chat_id 
                          FROM users 
                          WHERE users.chat_id == {chat_id}
                    """
        execute_sql_command(cursor, command_sql)
        results = cursor.fetchall()
        if len(results) == 0:
            return False
        return True


@sql_error_handling
def add_user(hotels_base: str, chat_id: int) -> None:
    """
    Функция добавляет нового пользователя, перед добавлением пользователь ищется по chat_id
    Если пользователь найден, то не добавляется
    :param hotels_base: файл базы данных
    :param chat_id: id чата
    :return: None
    """
    logger.info("Function 'add_user' is executed")
    connect = connect_to_base(hotels_base)
    with connect:
        cursor = connect.cursor()
        command_sql = f"""SELECT users.chat_id 
                          FROM users 
                          WHERE users.chat_id == {chat_id}"""
        execute_sql_command(cursor, command_sql)
        results = cursor.fetchall()
        if len(results) == 0:
            command_sql = f"""INSERT INTO users (chat_id) VALUES ({chat_id})"""
            execute_sql_command(cursor, command_sql)


@sql_error_handling
def set_hotel(hotels_base: str, chat_id: int, name: str, address: str, landmark: str, urls: str, price_one_day: str,
              price_total: str, id_hotel: int) -> None:
    """
    Функция добавляет отель в базу данных
    :param hotels_base: файл базы данных
    :param chat_id: id чата
    :param name: название отеля
    :param address: адресс отеля
    :param landmark: точка отсчета расcтояния
    :param urls: URL
    :param price_one_day: цена за один день
    :param price_total: цена за весь период
    :param id_hotel: id отеля
    :return: None
    """
    logger.info("Function 'set_hotel' is executed")
    user_request_id = get_user_request_id(hotels_base, chat_id)
    connect = connect_to_base(hotels_base)
    with connect:
        cursor = connect.cursor()
        command_sql = f'''
                INSERT INTO hotels (
                    request_id, address,
                    id_hotel, landmark,
                    name, price_one_day,
                    price_total, urls) VALUES (
                    {user_request_id}, "{address}",
                    {id_hotel}, "{landmark}",
                    "{name}", "{price_one_day}",
                    "{price_total}", "{urls}"
                    )
                '''
        execute_sql_command(cursor, command_sql)


@sql_error_handling
def get_requests_by_chat_id(hotels_base: str, chat_id: int) -> list:
    """
    Функция возвращает список запросов пользователя
    :param hotels_base:  база данных
    :param chat_id:  id пользователя
    :return: список запросов пользователя
    """
    connect = connect_to_base(hotels_base)
    logger.info("Function 'get_requests_by_chat_id' is executed")
    with connect:
        cursor = connect.cursor()
        command_sql = f"""
                SELECT * FROM requests
                WHERE requests.chat_id == {chat_id}
                """
        execute_sql_command(cursor, command_sql)
        request_history = cursor.fetchall()
        return request_history


@sql_error_handling
def get_hotels_by_request_id(hotels_base: str, request_id: int):
    """
    Функция возвращает список отелей по id запроса
    :param hotels_base:  база данных
    :param request_id: id запроса
    :return:
    """
    connect = connect_to_base(hotels_base)
    with connect:
        cursor = connect.cursor()
        command_sql = f"""
                SELECT * FROM hotels
                WHERE hotels.request_id == {request_id}
                """
        execute_sql_command(cursor, command_sql)
        hotels = cursor.fetchall()
        return hotels


@sql_error_handling
def get_users(hotels_base: str) -> list:
    """
    Функция возвращает данные всех пользователей
    :param hotels_base: база данных
    :return: список с данными польователей
    """
    logger.info("Function 'get_users' is executed")
    connect = connect_to_base(hotels_base)
    with connect:
        cursor = connect.cursor()
        command_sql = "SELECT * FROM users"
        execute_sql_command(cursor, command_sql)
        users = cursor.fetchall()
        return users


@sql_error_handling
def set_user_step(hotels_base: str, chat_id: int, step: int) -> None:
    """
    Функция задает текущий шаг запроса пользователя в Телеграм
    :param hotels_base: база данных
    :param chat_id: id пользователя
    :param step: номер шага
    :return: None
    """
    logger.info("Function 'set_user_step' is executed")
    connect = connect_to_base(hotels_base)
    with connect:
        cursor = connect.cursor()
        command_sql = f"""UPDATE users SET step_current = {step} 
                          WHERE chat_id = {chat_id}"""
        execute_sql_command(cursor, command_sql)

@sql_error_handling
def get_user_step(hotels_base: str, chat_id: int) -> int:
    """
    Функция возвращает текущий шаг запроса пользователя в Телеграмм
    :param hotels_base: база данных
    :param chat_id: id пользователя
    :return: номер текущего шага запроса пользователя в Телеграмм
    """
    logger.info("Function 'get_user_step' is executed")
    connect = connect_to_base(hotels_base)
    with connect:
        cursor = connect.cursor()
        command_sql = f"""SELECT users.step_current 
                          FROM users 
                          WHERE users.chat_id = {chat_id}"""
        execute_sql_command(cursor, command_sql)
        user_step = cursor.fetchall()[0][0]
        return user_step


@sql_error_handling
def set_user_request_id(hotels_base: str, chat_id: int, request_id: int) -> None:
    """
    Функция задает id запроса для пользователя через id пользователя
    :param hotels_base: база данных
    :param chat_id: id пользователя
    :param request_id: номер запроса
    :return: None
    """
    logger.info("Function 'set_user_request_id' is executed")
    connect = connect_to_base(hotels_base)
    with connect:
        cursor = connect.cursor()
        command_sql = f"""UPDATE users SET request_id = {request_id} 
                          WHERE chat_id = {chat_id}"""
        execute_sql_command(cursor, command_sql)


@sql_error_handling
def get_user_request_id(hotels_base: str, chat_id: int) -> int:
    """
    Функция возвращает текущий номер запроса для пользователя с номером chat_id
    :param hotels_base: база данных
    :param chat_id: id пользователя
    :return: номер запроса
    """
    logger.info("Function 'get_user_request_id' is executed")
    connect = connect_to_base(hotels_base)
    with connect:
        cursor = connect.cursor()
        command_sql = f"""SELECT users.request_id 
                          FROM users 
                          WHERE users.chat_id = {chat_id}"""
        execute_sql_command(cursor, command_sql)
        request_id = cursor.fetchall()[0][0]
        return request_id


@sql_error_handling
def set_command(hotels_base: str, chat_id: int, command: str) -> int:
    """
    Функция добавляет команды (lowprice, highprice, bestprice)
    :param hotels_base: база данных
    :param chat_id: номер пользователя
    :param command: команда
    :return: номер последнего запроса
    """
    logger.info("Function 'set_command' is executed")
    connect = connect_to_base(hotels_base)
    with connect:
        cursor = connect.cursor()
        command_sql = f"""
                INSERT INTO requests (
                    chat_id, command,
                    date_command, dates,
                    distances, hotels_amount,
                    picture_amount, prices) VALUES (
                    {chat_id}, '{command}',
                    '', '',
                    '', 0,
                    0, '')
                """
        execute_sql_command(cursor, command_sql)
        request_id = cursor.lastrowid
        return request_id


@sql_error_handling
def get_command(hotels_base: str, chat_id: int) -> str:
    """
    Функция возвращает текущую команду пользователя
    :param hotels_base: база данных
    :param chat_id: номер пользователя
    :return: текущая команда
    """
    logger.info("Function 'get_command' is executed")
    connect = connect_to_base(hotels_base)
    with connect:
        cursor = connect.cursor()
        command_sql = f"""
                SELECT requests.command FROM requests, users
                WHERE users.chat_id == {chat_id} AND users.request_id == requests.id
        """
        execute_sql_command(cursor, command_sql)
        command_search = cursor.fetchall()
        return command_search[0][0]


@sql_error_handling
def get_request_current(hotels_base: str, chat_id: int) -> list:
    """
    Функция возвращает текущий (последний) запрос пользователя с кодом chat_id
    :param hotels_base: база данных
    :param chat_id: id пользователя
    :return:  текущий (последний) запрос пользователя
    """
    logger.info("Function 'get_request_current' is executed")
    connect = connect_to_base(hotels_base)
    with connect:
        cursor = connect.cursor()
        command_sql = f"""
                    SELECT requests.command, 
                           requests.dates,
                           requests.city,
                           requests.destination_id,
                           requests.prices,                                                      
                           requests.hotels_amount,
                           requests.picture_amount,
                           requests.distances
                    FROM requests, users
                    WHERE users.chat_id == {chat_id} AND users.request_id == requests.id
            """
        execute_sql_command(cursor, command_sql)
        command_search = cursor.fetchall()
        return command_search[0]


@sql_error_handling
def set_city(hotels_base: str, chat_id: int, city: str) -> None:
    """
    Функция сохраняет город для последнего запроса пользователя с кодом chat_id
    :param hotels_base: база данных
    :param chat_id: id пользователя
    :param city: город запроса
    :return: None
    """
    logger.info("Function 'set_city' is executed")
    user_request_id = get_user_request_id(hotels_base, chat_id)
    connect = connect_to_base(hotels_base)
    with connect:
        cursor = connect.cursor()
        command_sql = f"""
                UPDATE requests SET city = '{city}' 
                WHERE  requests.chat_id == {chat_id} AND requests.id == {user_request_id} 
               """
        execute_sql_command(cursor, command_sql)


@sql_error_handling
def set_destination_id(hotels_base: str, chat_id: int, destination_id: str) -> None:
    """
    Функция сохраняет город для последнего запроса пользователя с кодом chat_id
    :param hotels_base: база данных
    :param chat_id: id пользователя
    :param city: город запроса
    :return: None
    """
    logger.info("Function 'set_city' is executed")
    user_request_id = get_user_request_id(hotels_base, chat_id)
    connect = connect_to_base(hotels_base)
    with connect:
        cursor = connect.cursor()
        command_sql = f"""
                UPDATE requests SET destination_id = '{destination_id}' 
                WHERE  requests.chat_id == {chat_id} AND requests.id == {user_request_id} 
               """
        execute_sql_command(cursor, command_sql)


@sql_error_handling
def get_destination_id(hotels_base: str, chat_id: int) -> str:
    """
    Функция возвращает диапазон дат поиска отелей
    :param hotels_base: база данных
    :param chat_id: номер пользователя
    :return: диапазон дат поиска отелей
    """
    logger.info("Function 'get_destination_id' is executed")
    user_request_id = get_user_request_id(hotels_base, chat_id)
    connect = connect_to_base(hotels_base)
    with connect:
        cursor = connect.cursor()
        command_sql = f"""
                    SELECT destination_id 
                    FROM requests 
                    WHERE  requests.chat_id == {chat_id} AND requests.id == {user_request_id}
                    """
        execute_sql_command(cursor, command_sql)
        dates = cursor.fetchall()
        return dates[0][0]

@sql_error_handling
def set_dates(hotels_base: str, chat_id: int, dates: str) -> None:
    """
    Функция сохраняет даты въезда/выезда из отеля последнего запроса пользователя с кодом chat_id
    :param hotels_base: база данных
    :param chat_id: id пользователя
    :param dates: даты въезда/выезда
    :return: None
    """
    logger.info("Function 'set_dates' is executed")
    user_request_id = get_user_request_id(hotels_base, chat_id)
    connect = connect_to_base(hotels_base)
    with connect:
        cursor = connect.cursor()
        command_sql = f"""
                UPDATE requests SET dates = '{dates}' 
                WHERE  requests.chat_id == {chat_id} AND requests.id == {user_request_id} 
               """
        execute_sql_command(cursor, command_sql)


@sql_error_handling
def get_dates(hotels_base: str, chat_id: int) -> str:
    """
    Функция возвращает диапазон дат поиска отелей
    :param hotels_base: база данных
    :param chat_id: номер пользователя
    :return: диапазон дат поиска отелей
    """
    logger.info("Function 'get_dates' is executed")
    user_request_id = get_user_request_id(hotels_base, chat_id)
    connect = connect_to_base(hotels_base)
    with connect:
        cursor = connect.cursor()
        command_sql = f"""
                    SELECT dates 
                    FROM requests 
                    WHERE  requests.chat_id == {chat_id} AND requests.id == {user_request_id}
                    """
        execute_sql_command(cursor, command_sql)
        dates = cursor.fetchall()
        return dates[0][0]


@sql_error_handling
def set_distances(hotels_base: str, chat_id: int, distances: str) -> None:
    """
    Функция сохраняет диапазон расстояний от центра городо до отеля в последнем запросе пользователя с кодом chat_id
    :param hotels_base: база данных
    :param chat_id: номер пользователя
    :param distances: диапазон расстояний от центра городо до отеля
    :return: None
    """
    logger.info("Function 'set_distances' is executed")
    user_request_id = get_user_request_id(hotels_base, chat_id)
    connect = connect_to_base(hotels_base)
    with connect:
        cursor = connect.cursor()
        command_sql = f"""
                UPDATE requests SET distances = '{distances}' 
                WHERE  requests.chat_id == {chat_id} AND requests.id == {user_request_id} 
               """
        execute_sql_command(cursor, command_sql)


@sql_error_handling
def set_hotels_amount(hotels_base: str, chat_id: int, hotels_amount: int) -> None:
    """
    Функция сохраняет количество отелей в последнем запросе пользователя с кодом chat_id
    :param hotels_base: база данных
    :param chat_id: номер пользователя
    :param hotels_amount:
    :return: None
    """
    logger.info("Function 'set_hotels_amount' is executed")
    user_request_id = get_user_request_id(hotels_base, chat_id)
    connect = connect_to_base(hotels_base)
    with connect:
        cursor = connect.cursor()
        command_sql = f"""
                UPDATE requests SET hotels_amount = {hotels_amount} 
                WHERE  requests.chat_id == {chat_id} AND requests.id == {user_request_id} 
                """
        execute_sql_command(cursor, command_sql)


@sql_error_handling
def set_picture_amount(hotels_base: str, chat_id: int, picture_amount: int) -> None:
    """
    Функция сохраняет количество фотографий в последнем запросе пользователя с кодом chat_id
    :param hotels_base: база данных
    :param chat_id: номер пользователя
    :param picture_amount:
    :return: None
    """
    logger.info("Function 'set_picture_amount' is executed")
    user_request_id = get_user_request_id(hotels_base, chat_id)
    connect = connect_to_base(hotels_base)
    with connect:
        cursor = connect.cursor()
        command_sql = f"""
                UPDATE requests SET picture_amount = {picture_amount} 
                WHERE  requests.chat_id == {chat_id} AND requests.id == {user_request_id} 
                """
        execute_sql_command(cursor, command_sql)


@sql_error_handling
def set_prices(hotels_base: str, chat_id: int, prices: str) -> None:
    """
    Функция сохраняет диапаозон цен в последнем запросе пользователя с кодом chat_id
    :param hotels_base: база данных
    :param chat_id: номер пользователя
    :param prices: диапаозон цен
    :return: None
    """
    logger.info("Function 'set_prices' is executed")
    user_request_id = get_user_request_id(hotels_base, chat_id)
    connect = connect_to_base(hotels_base)
    with connect:
        cursor = connect.cursor()
        command_sql = f"""
                UPDATE requests SET prices = '{prices}'
                WHERE  requests.chat_id == {chat_id} AND requests.id == {user_request_id} 
                """
        execute_sql_command(cursor, command_sql)

