import requests

import config
import sql_work
from utils import get_days_amount

from config import HEADERS
from config import URL_SEARCH_HOTELS
from config import URL_DESTINATION
from config import URL_PHOTOS
from config import DEVELOPER_EMAIL

from config_log import logger


# def get_destinationId(destinations: dict, city: str) -> list:
#     """
#     Функция ищет в словаре и возвращает "destinationId" для заданного города
#     :param destinations:
#     :param city:
#     :return:
#     """
#     logger.info("Function 'get_destinationId' is executed")
#     try:
#         destinationIds = list()
#         if isinstance(destinations, dict):
#             suggestions = destinations.get("suggestions", None)
#             if suggestions:
#                 for value in suggestions[0]["entities"]:
#                     if value["name"] == city:
#                         destinationIds.append(value['destinationId'])
#         return destinationIds
#     except (IndexError, KeyError) as error:
#         logger.error(f"Error in function 'get_destinationId': {error.args[0]}")
#         raise IndexError(f"Ошибка в функции 'get_destinationId': {error.args[0]}.\n"
#                          f"Пожалуйста, сообщите о ней разработчику по адресу {DEVELOPER_EMAIL}.") from error


def filter_destinations(destinations: dict, city: str) -> list:
    """
    Функция ищет в словаре и возвращает "destinationId" для заданного города
    :param destinations:
    :param city:
    :return:
    """
    logger.info("Function 'get_destinationId' is executed")
    try:
        destinations_ = list()
        if isinstance(destinations, dict):
            suggestions = destinations.get("suggestions", None)
            for suggestion in suggestions:
                if suggestion['group'] == "CITY_GROUP":
                    for entity in suggestion["entities"]:
                        if entity["name"] == str.title(city) or entity["caption"].find(str.title(city)) >= 0:
                            destination = {"id": entity['destinationId'], "caption": entity['caption'],
                                           "name": entity['name']}
                            destinations_.append(destination)

        return destinations_
    except (IndexError, KeyError) as error:
        logger.error(f"Error in function 'get_destinationId': {error.args[0]}")
        raise IndexError(f"Ошибка в функции 'get_destinationId': {error.args[0]}.\n"
                         f"Пожалуйста, сообщите о ней разработчику по адресу {DEVELOPER_EMAIL}.") from error


def get_search_hotels_query(
        checkin_date: str,
        checkout_date: str,
        destination_id: str,
        sort_order: str,
        price_min: str,
        price_max: str) -> dict:
    """
    Фунция возвращает входные данные для поиска отелей
    :param checkin_date: str = "YYYY-mm-dd"
    :param checkout_date: str = "YYYY-mm-dd"
    :param destination_id: str, unique id
    :param sort_order: str, "PRICE"
    :param price_min: str
    :param price_max: str
    :return query_search_hotels: dict
    """
    logger.info("Function 'get_search_hotels_query' is executed")
    try:
        query_search_hotels = {
            "checkin_date": checkin_date,
            "checkout_date": checkout_date,
            "sort_order": sort_order,
            "destination_id": destination_id,
            "adults_number": "1",
            "locale": "en_US",
            "currency": "USD",
            "price_min": price_min,
            "price_max": price_max
        }
        return query_search_hotels
    except TypeError as error:
        logger.error(f"Error in function 'get_search_hotels_query': {error.args[0]}")
        raise IndexError(f"Ошибка в функции 'get_search_hotels_query': {error.args[0]}.\n"
                         f"Пожалуйста, сообщите о ней разработчику по адресу {DEVELOPER_EMAIL}.") from error


def get_hotels_from_dict(hotels: dict,
                  days_amount: int,
                  amount_hotels: int,
                  distanse_min: float,
                  distanse_max: float) -> list:
    """
    Функция возвращает список отелей
    :param hotels: словарь полученый по запросу
    :param days_amount: на сколько дней заказан отель
    :param price_type: "lowprice", "highprice" или "bestdeal"
    :param amount_hotels: максимальное количество отелей
    :return: список отелей с их параметрами
    """
    logger.info("Function 'get_hotels_from_dict' is executed")
    try:
        hotels_result = list()
        if isinstance(hotels, dict) and len(hotels) > 0:
            results = hotels["searchResults"]["results"]
            for result in results:
                distanse = float(result['landmarks'][0]['distance'].split(" ")[0])

                if distanse_min <= distanse <= distanse_max:
                    hotel = dict()

                    hotel_name = result['name']
                    hotel_url = f"hotels.com/ho{result['id']}"
                    hotel["name"] = f"[{hotel_name}]({hotel_url})"
                    hotel["address"] = f"{result['address'].get('locality', '')}, {result['address'].get('streetAddress', '')}"
                    hotel["landmark"] = result['landmarks'][0]['distance']
                    hotel["urls"] = hotel_url
                    hotel["price_one_day"] = result['ratePlan']['price']['current']

                    curr_sign = result['ratePlan']['price']['current'][0]
                    price_one_day = result['ratePlan']['price']['current'][1:].replace(',', '.')
                    price_one_day = float(price_one_day)
                    hotel["price_total"] = curr_sign + str(price_one_day * days_amount)

                    hotel["id"] = result['id']
                    hotels_result.append(hotel)

            hotels_result = hotels_result[:amount_hotels]

        return hotels_result
    except (IndexError, KeyError) as error:
        logger.error(f"Error in function 'get_hotels_from_dict': {error.args[0]}")
        raise IndexError(f"Ошибка в функции 'get_hotels_from_dict': {error.args[0]}.\n"
                         f"Пожалуйста, сообщите о ней разработчику по адресу {DEVELOPER_EMAIL}.") from error


def get_hotels_photos_by_request(hotel_id: str, amount_max: int) -> list:
    """
    Функция возращает список адресов с фото отеля
    :param hotel_id: id отеля
    :param amount_max: максимальное количество фотографий
    :return: список адресов с фото отеля
    """
    logger.info("Function 'get_hotels_photos_by_request' is executed")
    try:
        querystring_photo = {"hotel_id": hotel_id}
        response = requests.request("GET", URL_PHOTOS, headers=HEADERS, params=querystring_photo)
        photos = response.json()
        photo_addresses = []
        for photo in photos[:amount_max]:
            photo_addresses.append(photo["mainUrl"])
        return photo_addresses
    except (IndexError, KeyError) as error:
        logger.error(f"Ошибка в функции 'get_hotels_photos_by_request': {error.args[0]}")
        raise IndexError(f"Ошибка в функции 'get_hotels_photos_by_request': {error.args[0]}.\n"
                         f"Пожалуйста, сообщите о ней разработчику по адресу {DEVELOPER_EMAIL}.") from error
    except requests.exceptions.RequestException as error:
        logger.error(f"Ошибка в функции 'get_hotels_photos_by_request': {error.args[0]}")
        raise requests.exceptions.RequestException(
            f"Ошибка в функции 'get_hotels_photos_by_request': {error.args[0]}.\n") from error


def get_destinations(city: str) -> list[dict]:
    currency = "USD"
    locale = "en_US"

    querystring_destination_id = {
        "query": city,
        "currency": currency,
        "locale": locale
    }
    cities = requests.request("GET", URL_DESTINATION, headers=HEADERS, params=querystring_destination_id).json()
    destinations = filter_destinations(cities, city)
    return destinations


def get_hotels_by_request(
        headers: dict,
        city: str,
        checkin_date: str,
        checkout_date: str,
        price_min: str,
        price_max: str,
        price_sorting: str,
        hotels_amount: int,
        distance_min: float,
        distance_max: float,
        chat_id: str,
        destination_id: str) -> list:
    """
    Функция возвращает описание выбраных отелей и их названия
    :param headers:  headers = {"X-RapidAPI-Host": XXX, "X-RapidAPI-Key": YYY}
    :param city: город в котором ищутся отели
    :param checkin_date: начальная дата поиска отеля (YYYY-mm-dd)
    :param checkout_date: конечная дата поиска отеля (YYYY-mm-dd)
    :param price_min: Минимальная цена за сутки
    :param price_max: Максимальная цена за сутки
    :param price_sorting: минимальные цены ("lowprice"), максимальная цена ("highprice")
    :param hotels_amount: Максимальне количество отелей в выборке
    :param distance_min: Минимальное расстояние до отеля
    :param distance_max: Максимальное рассстояние до отеля
    :return: hotels_list - список с полным описанием отелей
    """
    logger.info("Function 'get_hotels_by_request' is executed")
    try:
        # if price_sorting == "lowprice" or price_sorting == "bestdeal":
        if price_sorting in ["lowprice", "bestdeal"]:
            sort_order = "PRICE"
        else:
            sort_order = "PRICE_HIGHEST_FIRST"

        if len(destination_id):
            query_search_hotels = get_search_hotels_query(checkin_date, checkout_date, destination_id, sort_order,
                                                          price_min, price_max)
            hotels = requests.request("GET", URL_SEARCH_HOTELS, headers=headers, params=query_search_hotels).json()
            days_amount = get_days_amount(checkin_date, checkout_date)
            hotels_list = get_hotels_from_dict(hotels, days_amount, hotels_amount, distance_min,
                                               distance_max)
            return hotels_list
        return list()
    except IndexError as error:
        logger.error(f"Ошибка в функции 'get_hotels_by_request': {error.args[0]}")
        raise IndexError(f"Ошибка в функции 'get_hotels_by_request': {error.args[0]}.\n"
                         f"Пожалуйста, сообщите о ней разработчику по адресу {DEVELOPER_EMAIL}.") from error
    except requests.exceptions.RequestException as error:
        logger.error(f"Ошибка в функции 'get_hotels_by_request': {error.args[0]}")
        raise requests.exceptions.RequestException(
            f"Ошибка в функции 'get_hotels_by_request': {error.args[0]}.\n") from error
